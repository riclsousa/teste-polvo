function validarSenha(){
	var senhaJs = document.getElementById("inputPassword1").value;
	var forca = 0;
	var condMaiuscula = /[A-Z]+/;
	var condNumero = /[0-9]+/;

	if (senhaJs.length == 0){
		forca = 0;
	}else if (senhaJs.length > 5){
		forca = forca + 1;
	}
	
	if (condMaiuscula.exec(senhaJs)) {
		forca = forca + 5;
	}
	
	if (condNumero.exec(senhaJs)){
		forca = forca + 10;
	}

	if (forca == 0){
		document.getElementById("validacaoA1").style.background = "#EDEDED"; 
		document.getElementById("validacaoA2").style.background = "#EDEDED"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#828282"; 
		document.getElementById("cond2").style.color = "#828282"; 
		document.getElementById("cond3").style.color = "#828282"; 
		document.getElementById("inputPassword1").style.border = "1px solid #A9A9A9";
	} else if (forca == 1){
		document.getElementById("validacaoA1").style.background = "#F26722"; 
		document.getElementById("validacaoA2").style.background = "#EDEDED"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#1ED699"; 
		document.getElementById("cond2").style.color = "#F26722"; 
		document.getElementById("cond3").style.color = "#F26722"; 
		document.getElementById("inputPassword1").style.border = "1px solid #F26722";
	}else if (forca == 5){
		document.getElementById("validacaoA1").style.background = "#F26722"; 
		document.getElementById("validacaoA2").style.background = "#EDEDED"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#F26722"; 
		document.getElementById("cond2").style.color = "#1ED699"; 
		document.getElementById("cond3").style.color = "#F26722"; 
		document.getElementById("inputPassword1").style.border = "1px solid #F26722";
	}else if (forca == 6){
		document.getElementById("validacaoA1").style.background = "#F2B822"; 
		document.getElementById("validacaoA2").style.background = "#F2B822"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#1ED699"; 
		document.getElementById("cond2").style.color = "#1ED699"; 
		document.getElementById("cond3").style.color = "#F26722"; 
		document.getElementById("inputPassword1").style.border = "1px solid #F2B822";
	}else if (forca == 10){
		document.getElementById("validacaoA1").style.background = "#F26722"; 
		document.getElementById("validacaoA2").style.background = "#EDEDED"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#F26722"; 
		document.getElementById("cond2").style.color = "#F26722"; 
		document.getElementById("cond3").style.color = "#1ED699"; 
		document.getElementById("inputPassword1").style.border = "1px solid #F26722";
	}else if (forca == 11){
		document.getElementById("validacaoA1").style.background = "#F2B822"; 
		document.getElementById("validacaoA2").style.background = "#F2B822"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#1ED699";
		document.getElementById("cond2").style.color = "#F26722";
		document.getElementById("cond3").style.color = "#1ED699";
		document.getElementById("inputPassword1").style.border = "1px solid #F2B822";
	}else if (forca == 15) {
		document.getElementById("validacaoA1").style.background = "#F2B822"; 
		document.getElementById("validacaoA2").style.background = "#F2B822"; 
		document.getElementById("validacaoA3").style.background = "#EDEDED"; 
		document.getElementById("cond1").style.color = "#F26722";
		document.getElementById("cond2").style.color = "#1ED699";
		document.getElementById("cond3").style.color = "#1ED699";
		document.getElementById("inputPassword1").style.border = "1px solid #F2B822";
	}else if (forca == 16){
		document.getElementById("validacaoA1").style.background = "#1ED699"; 
		document.getElementById("validacaoA2").style.background = "#1ED699"; 
		document.getElementById("validacaoA3").style.background = "#1ED699"; 
		document.getElementById("cond1").style.color = "#1ED699";
		document.getElementById("cond2").style.color = "#1ED699";
		document.getElementById("cond3").style.color = "#1ED699";
		document.getElementById("inputPassword1").style.border = "1px solid #1ED699";
	}
}